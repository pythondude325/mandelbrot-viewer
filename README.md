# Mandelbrot set viewer
This is a webgl based mandlebrot viewer. The goal of this was to make a
mandelbrot set viewer that was very fast with GPU acceleration. It uses the
[GlslCanvas](https://github.com/patriciogonzalezvivo/glslCanvas) library to
interface with WebGL.
