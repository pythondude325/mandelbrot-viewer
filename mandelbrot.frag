#ifdef GL_ES
precision highp float;
#endif

#extension GL_OES_standard_derivatives : enable

uniform float u_time;
uniform vec2 u_mouse;
uniform vec2 u_resolution;

uniform float u_scale;
uniform vec2 u_offset;
uniform float u_max_iterations;
uniform float u_smoothing;

const float B = 256.0;

vec3 gray_colors(float i){
  return vec3(i);
}

vec3 fire_colors(float i){
  return vec3(i*2.,i*2.-1.,i*2.-1.);
}

void main( void ) {
  float scale = pow(2.0, -u_scale);

  vec2 c = ((gl_FragCoord.xy - u_resolution / 2.0)/ u_resolution.y) * scale + u_offset;
  vec2 z = vec2(0.0);
  float n = 0.0;

  for(float i = 0.0; i < 1e5; i++){
    if(i > u_max_iterations) break;
    z = vec2(z.x*z.x - z.y*z.y, 2.0*z.x*z.y) + c;
    if(dot(z,z) >= (B*B)) break;
    n++;
  }

  float o = 0.0;

  if(n < u_max_iterations){
    if(u_smoothing > 0.0) n = n - log2(log2(dot(z,z))) + 4.0;
    float l = n / u_max_iterations;

    o = pow(l, 0.5) / 1.5;
  } else {
    o = 1.0;
  }

  gl_FragColor = vec4(gray_colors(o), 1.0);
}