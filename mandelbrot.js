/* global GlslCanvas */

(async function(){
  let [canvas, zoomLabel, offsetLabel, detailLabel] = ["main-canvas", "zoom-label", "offset-label", "detail-label"].map(i => document.getElementById(i))

  let sandbox = new GlslCanvas(canvas)

  const shader = await fetch("mandelbrot.frag").then(r => r.text())
  sandbox.load(shader)

  //sandbox.load(shader)

  let scale = -2, xOffset = -0.5, yOffset = 0, detailLevel = 1e2, smoothing = true

  function getZoomLevel(){
    return 2**(-scale)
  }

  function draw(){
    sandbox.refreshUniforms()
    sandbox.setUniform("u_scale", scale)
    sandbox.setUniform("u_offset", xOffset, yOffset)
    sandbox.setUniform("u_max_iterations", detailLevel)
    sandbox.setUniform("u_smoothing", smoothing ? 1 : 0)

    zoomLabel.innerText = scale.toString()
    offsetLabel.innerText = `${xOffset.toFixed(7)} + ${yOffset.toFixed(7)}i`
    detailLabel.innerText = detailLevel.toExponential()
  }

  draw()

  const scale_change = 0.25, detail_change = 1.1

  window.addEventListener("keypress", e => {
    const d = 0.1 * getZoomLevel()
    switch(e.key){
    case "w":
      yOffset += d
      break
    case "s":
      yOffset -= d
      break
    case "a":
      xOffset -= d
      break
    case "d":
      xOffset += d
      break
    case ".":
      scale += scale_change
      break
    case ",":
      scale -= scale_change
      break
    case "[":
      detailLevel /= detail_change
      break
    case "]":
      detailLevel *= detail_change
      break
    case "p":
      smoothing = !smoothing
      break
    }

    draw()
  })

})()
